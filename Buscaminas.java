import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
/**
 * Clase que representa el Buscaminas
 * @author Cisco
 * @version 1.0
 */

public class Buscaminas {


	static Scanner sc = new Scanner(System.in);

	static Random r = new Random();

	public static void main(String[] args) {
		/**
		 * Acaba de jugar, sirve para cerrar el menú i parar el programa
		 */
		boolean fi = false;
		
		/**
		 * Dice si ganas
		 */
		boolean win = false;
		
		/**
		 * Dice si pierdes
		 */
		boolean lose = false;
		
		/**
		 * Nombre del jugador
		 */
		String nom = "j1";
		
		/**
		 * Número de filas del mapa
		 */
		int f = 1;
		
		/**
		 * Número de columnas del mapa
		 */
		int c = 1;
		
		/**
		 * Número de minas en el mapa
		 */
		int minas = 0;
		
		/**
		 * Lista de ganadores
		 */
		ArrayList<String> ganadores = new ArrayList<>();
		
		while (!fi) {
			/**
			 * Menú del juego
			 */
			int op = menu();
			switch (op) {
			case 1:
				System.out.println(
						"El juego consiste en despejar todas las casillas de una pantalla que no oculten una mina.");
				System.out.println(
						"Algunas casillas tienen un número, este número indica las minas que son en todas las casillas circundantes.");
				System.out.println(
						"Si se descubre una casilla con una mina se pierde la partida, si se descubren todas las casillas ganas.\n");
				break;
			case 2:
				System.out.println("Escribe tu nombre de Jugador");
				nom = obtenirNom(nom);
				System.out.println("Pon el tamaño del mapa");
				f = sc.nextInt();
				c = sc.nextInt();
				System.out.println("Pon el Número de minas");
				minas = sc.nextInt();
				if (minas >= (f * c)) {
					while (minas >= (f * c)) {
						System.out.println("Pon un Número de minas adecuado");
						minas = sc.nextInt();
					}
				}
				break;
			case 3:
				int[][] mat = new int[f][c];
				int[][] matSabes = new int[f][c];
				initMat(mat, matSabes, minas);
				printMat(matSabes);
				while (!win && !lose) {
					lose = pedirPosicion(mat, matSabes);
					win = comprobarVictoria(matSabes, mat);
					printMat(matSabes);
				}
				if (lose) {
					System.out.println("Mueres");
					lose = false;
				}
					
				if (win) {
					System.out.println("Ganaste");
					ganadores.add(nom);
					win = false;
				}

				break;
			case 4:
				System.out.println(ganadores);
				break;
			case 0:
				fi = true;
			}
		}

		System.out.println("A reveure!!");

	}

	/**
	 * Funcion que recorre el mapa real y el mapa que ves para saber si has ganado
	 * @param matSabes mapa que tu ves
	 * @param mat mapa real
	 * @return si hay casillas por descubrir que no tienen mina devuleve false, si no true
	 */
	private static boolean comprobarVictoria(int matSabes[][], int mat[][]) {
		// TODO Auto-generated method stub
		boolean flag = true;
		for (int i = 0; i < matSabes.length; i++) {
			for (int j = 0; j < matSabes[i].length; j++) {
				if (matSabes[i][j] == 9 && mat[i][j] == 0) {
					flag = false;
				}
			}
		}
		return flag;
	}

	/**
	 * Funcion que te pide la posición para destapar
	 * @param mat mapa real
	 * @param matSabes mapa que tu ves
	 * @return si la posición es una mina true, si no false
	 */
	private static boolean pedirPosicion(int mat[][], int matSabes[][]) {
		// TODO Auto-generated method stub
		boolean flag = false;
		int contador = 0;
		int fsabes = sc.nextInt();
		int csabes = sc.nextInt();
		if (estoyDentro(mat, fsabes, csabes) && mat[fsabes][csabes] == 1) {
			flag = true;
		} else if (estoyDentro(mat, fsabes, csabes) && mat[fsabes][csabes] == 0) {
			Destapar(matSabes, mat, fsabes, csabes);
			if (estoyDentro(mat, fsabes-1, csabes) && mat[fsabes-1][csabes] == 1) {
				contador++;
			}
			if (estoyDentro(mat, fsabes+1, csabes) && mat[fsabes+1][csabes] == 1) {
				contador++;
			}
			if (estoyDentro(mat, fsabes-1, csabes-1) && mat[fsabes-1][csabes-1] == 1) {
				contador++;
			}
			if (estoyDentro(mat, fsabes-1, csabes+1) && mat[fsabes-1][csabes+1] == 1) {
				contador++;
			}
			if (estoyDentro(mat, fsabes+1, csabes-1) && mat[fsabes+1][csabes-1] == 1) {
				contador++;
			}
			if (estoyDentro(mat, fsabes+1, csabes+1) && mat[fsabes+1][csabes+1] == 1) {
				contador++;
			}
			if (estoyDentro(mat, fsabes, csabes-1) && mat[fsabes][csabes-1] == 1) {
				contador++;
			}
			if (estoyDentro(mat, fsabes, csabes+1) && mat[fsabes][csabes+1] == 1) {
				contador++;
			}
			matSabes[fsabes][csabes] = contador;
		}
		return flag;
	}

	/**
	 * Funcion que destapa las casillas
	 * @param matSabes mapa que tu ves
	 * @param mat mapa real
	 * @param i fila seleccionada
	 * @param j columna seleccionada
	 */
	private static void Destapar(int[][] matSabes, int[][] mat, int i, int j) {
		// TODO Auto-generated method stub
		int contador=0;
		if (!estoyDentro(matSabes, i, j) || (estoyDentro(matSabes, i, j) && mat[i][j] == 1)
				|| (estoyDentro(matSabes, i + 1, j + 1) && mat[i + 1][j + 1] == 1)
				|| (estoyDentro(matSabes, i + 1, j - 1) && mat[i + 1][j - 1] == 1)
				|| (estoyDentro(matSabes, i - 1, j - 1) && mat[i - 1][j - 1] == 1)
				|| (estoyDentro(matSabes, i - 1, j + 1) && mat[i - 1][j + 1] == 1)
				
				|| (estoyDentro(matSabes, i - 1, j) && mat[i - 1][j] == 1)
				|| (estoyDentro(matSabes, i + 1, j) && mat[i + 1][j] == 1)
				
//				|| (estoyDentro(matSabes, i, j + 1) && mat[i][j + 1] == 1)
//				|| (estoyDentro(matSabes, i, j - 1) && mat[i][j - 1] == 1)
// Comento esto porque depende de como crees el mapa ganas al instante		
				|| matSabes[i][j] != 9)
			return;
		
		if(estoyDentro(matSabes, i + 1, j) && mat[i + 1][j] == 1)contador++;
		if(estoyDentro(matSabes, i - 1, j) && mat[i - 1][j] == 1)contador++;
		if(estoyDentro(matSabes, i + 1, j + 1) && mat[i + 1][j + 1] == 1)contador++;
		if(estoyDentro(matSabes, i + 1, j - 1) && mat[i + 1][j - 1] == 1)contador++;
		if(estoyDentro(matSabes, i - 1, j - 1) && mat[i - 1][j - 1] == 1)contador++;
		if(estoyDentro(matSabes, i, j + 1) && mat[i][j + 1] == 1)contador++;
		if(estoyDentro(matSabes, i, j - 1) && mat[i][j - 1] == 1)contador++;
		if(estoyDentro(matSabes, i - 1, j + 1) && mat[i - 1][j + 1] == 1)contador++;
		
		matSabes[i][j] = contador;
		
		Destapar(matSabes, mat, i - 1, j);
		Destapar(matSabes, mat, i + 1, j);
		Destapar(matSabes, mat, i - 1, j - 1);
		Destapar(matSabes, mat, i - 1, j + 1);
		Destapar(matSabes, mat, i + 1, j - 1);
		Destapar(matSabes, mat, i, j + 1);
		Destapar(matSabes, mat, i, j - 1);
		Destapar(matSabes, mat, i + 1, j + 1);
	}
	/**
	 * Funcion que imprime la matriz
	 * @param matSabes mapa que tu ves
	 */
	private static void printMat(int[][] matSabes) {
		// TODO Auto-generated method stub
		for (int i = 0; i < matSabes.length; i++) {
			for (int j = 0; j < matSabes[i].length; j++) {
				System.out.print(matSabes[i][j] + " ");
			}
			System.out.println();
		}
	}
	/**
	 * Funcion construye el mapa real y el que tu ves
	 * @param mat mapa real
	 * @param matSabes mapa que ves
	 * @param minas minas en el mapa
	 */
	private static void initMat(int[][] mat, int[][] matSabes, int minas) {
		// TODO Auto-generated method stub
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat.length; j++) {
				mat[i][j] = 0;
				matSabes[i][j] = 9;
			}
		}
		for (int j = 0; j < minas; j++) {
			int fminas = r.nextInt(0, mat.length);
			int cminas = r.nextInt(0, mat[0].length);
			if (mat[fminas][cminas] == 0) {
				mat[fminas][cminas] = 1;
			} else {
				j--;
			}
		}
	}
	/**
	 * Funcion que pone nombre al jugador
	 * @param nombre que le pones
	 * @return nombre que le has puesto
	 */
	private static String obtenirNom(String nom) {
		// TODO Auto-generated method stub
		nom = sc.nextLine();
		return nom;
	}
	
	/**
	 * Menú del juego
	 * @return La opción que quieres
	 */
	private static int menu() {
		int opcio = 0;
		System.out.println("***** BUSCAMINAS - MENÚ PRINCIPAL*****");
		System.out.println("ESCULL UNA OPCIÓ:");
		System.out.println("1- Mostrar Ajuda");
		System.out.println("2- Opcions");
		System.out.println("3- Jugar Partida");
		System.out.println("4- Veure Llista de Guanyadors");
		System.out.println("0- Sortir");
		opcio = sc.nextInt();
		sc.nextLine();
		return opcio;
	}
	
	/**
	 * Función que mira si estás dentro de la matriz
	 * @param mat matriz
	 * @param f fila de la matriz
	 * @param c columna de la matriz
	 * @return true si estás dentro, false si te sales
	 */
	public static boolean estoyDentro(int[][] mat, int f, int c) {
		return (f >= 0 && c >= 0 && f < mat.length && c < mat[0].length);
	}
}
