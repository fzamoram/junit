package Ejercicios;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class NumeroPrimoTest {

	@Test
	void test() {
		assertEquals(true, NumeroPrimo.Primer(2));
		assertEquals(true, NumeroPrimo.Primer(3));
		assertEquals(false, NumeroPrimo.Primer(0));
		assertEquals(false, NumeroPrimo.Primer(1));
		assertEquals(false, NumeroPrimo.Primer(4));
		assertEquals(false, NumeroPrimo.Primer(120));
		assertEquals(false, NumeroPrimo.Primer(77));
		assertEquals(false, NumeroPrimo.Primer(117));
		assertEquals(true, NumeroPrimo.Primer(131));
		assertEquals(true, NumeroPrimo.Primer(7));
		assertEquals(false, NumeroPrimo.Primer(6));
		assertEquals(true, NumeroPrimo.Primer(13));
		assertEquals(false, NumeroPrimo.Primer(18));
		assertEquals(true, NumeroPrimo.Primer(29));
		assertEquals(false, NumeroPrimo.Primer(27));
	}

}
