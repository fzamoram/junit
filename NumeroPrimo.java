package Ejercicios;

import java.util.Scanner;

public class NumeroPrimo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println(Primer(n));
	}

	public static boolean Primer(int n) {
		if (n == 1 || n == 0) {
			return false;
		} else {
			for (int i = 2; i < n - 1; i++) {
				if (n % i == 0)
					return false;
			}
		}
		return true;
	}
}
