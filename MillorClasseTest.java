package Ejercicios;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MillorClasseTest {
	final static String[][] MATRIZA = { { "·", "X", "X", "X", "·" }, { "X", "·", "·", "·", "X" },
			{ "X", "X", "X", "X", "X" }, { "X", "·", "·", "·", "X" }, { "X", "·", "·", "·", "X" } };

	final static String[][] MATRIZB = { { "X", "X", "X", "X", "·" }, { "X", "·", "·", "·", "X" },
			{ "X", "X", "X", "X", "·" }, { "X", "·", "·", "·", "X" }, { "X", "X", "X", "X", "·" } };

	final static String[][] MATRIZ = { { "·", "·", "·", "·", "·" }, { "·", "·", "·", "·", "·" },
			{ "·", "·", "·", "·", "·" }, { "·", "·", "·", "·", "·" }, { "·", "·", "·", "·", "·" } };

	@Test
	void test() {
		for (int i = 0; i < MATRIZB.length; i++) {
			for (int j = 0; j < MATRIZB.length; j++) {
				assertEquals(MATRIZA[i][j], MillorClasse.Classe(10, 1)[i][j]);
				assertEquals(MATRIZA[i][j], MillorClasse.Classe(3, 2)[i][j]);
				assertEquals(MATRIZA[i][j], MillorClasse.Classe(7, 4)[i][j]);
				assertEquals(MATRIZB[i][j], MillorClasse.Classe(1, 3)[i][j]);
				assertEquals(MATRIZB[i][j], MillorClasse.Classe(5, 6)[i][j]);
				assertEquals(MATRIZB[i][j], MillorClasse.Classe(7, 15)[i][j]);
				assertEquals(MATRIZB[i][j], MillorClasse.Classe(567, 700)[i][j]);
				assertEquals(MATRIZ[i][j], MillorClasse.Classe(10, 10)[i][j]);
				assertEquals(MATRIZ[i][j], MillorClasse.Classe(40, 40)[i][j]);
				assertEquals(MATRIZ[i][j], MillorClasse.Classe(3, 3)[i][j]);
			}
		}
	}

}
