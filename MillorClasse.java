package Ejercicios;

import java.util.Iterator;
import java.util.Scanner;

public class MillorClasse {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);	
		int a = sc.nextInt();
		int b = sc.nextInt();
		Classe(a, b);
	}

	public static String[][] Classe(int a, int b) {
		String[][] mat = new String[5][5];
		if (a > b) {
			for (int i = 0; i < mat.length; i++) {
				for (int j = 0; j < mat.length; j++) {
					if (i == 0 && (j == 0 || j == mat.length - 1)) {
						mat[i][j] = "·";
					} else if (i == 0 || j == 0 || j == mat.length - 1 || i == 2) {
						mat[i][j] = "X";
					} else {
						mat[i][j] = "·";
					}
					System.out.print(mat[i][j] + " ");
				}
				System.out.println();
			}

		} else if (a < b) {
			for (int i = 0; i < mat.length; i++) {
				for (int j = 0; j < mat.length; j++) {
					if ((i == 0 || i == 2 || i == mat.length - 1) && j == mat.length - 1) {
						mat[i][j] = "·";
					} else if (i == 0 || j == 0 || j == mat.length - 1 || i == 2 || i == mat.length - 1) {
						mat[i][j] = "X";
					} else {
						mat[i][j] = "·";
					}
					System.out.print(mat[i][j] + " ");
				}
				System.out.println();
			}
		} else {
			for (int i = 0; i < mat.length; i++) {
				for (int j = 0; j < mat.length; j++) {
						mat[i][j] = "·";
					System.out.print(mat[i][j] + " ");
				}
				System.out.println();
			}
		}
		return mat;
	}
}
